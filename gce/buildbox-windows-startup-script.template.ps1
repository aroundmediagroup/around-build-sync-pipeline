$timeStamp = [int64](([datetime]::UtcNow)-(get-date "1/1/1970")).TotalSeconds
Start-Transcript -path "C:\StartupScriptLogs\startuplog_$timeStamp.txt" -append
$originMachineName = Hostname
$p4Root = "D:\Perforce"
$engineSolutionPath = "$($pathToEngine)UE4.sln"
$engineBranchToSync = "Features/addChangesFromVXGI4.19"
Function Stream-Write-SerialPort {
    Begin {
        $port = new-Object System.IO.Ports.SerialPort COM1,9600,None,8,one
        $port.open()
    }
    process {
        Echo $_
        $port.WriteLine($_)
    }
    End {
        $port.Close()
    }
}

Function Write-SerialPort ([string] $message) {
    Echo $message | Stream-Write-SerialPort;
}

Function Exec-Then-SerialPort ([ScriptBlock] $block) {
    &{
        & $block
        if ( $LASTEXITCODE -ne 0 ) {
            Echo $Error[0].Exception
            Echo "Failure! Will shutdown now."
            Stop-Transcript
            Start-Sleep 5 ; Stop-Computer
        }
    } *>&1 | Stream-Write-SerialPort
}

Write-SerialPort ("Running startup script on $originMachineName")

# Set environment variables
$Env:P4PORT = "ssl:perforce.assembla.com:1667"
$Env:P4CLIENT = "jenkins-$originMachineName-Generic-0"
$Env:P4HOST = "aroundmedia/aroundmedia-first-space"
$Env:P4USER = "$p4User"
$Env:P4PASSWD = "$p4Password"
$Env:P4ROOT = "$p4Root"
$Env:P4CHARSET = "utf8"

# Establishing trust
Exec-Then-SerialPort { Echo "yes" | p4 trust }

# Print the last synced changelist for debug purposes
$lastChangeList = p4 changes -m1 //depot/main/SalesSuite...@$Env:P4CLIENT
Write-SerialPort ("Last synced changelist before sync of $originMachineName : $lastChangeList")

Write-SerialPort (" ")
Write-SerialPort ("----------------------------------------------------------------------------")
Write-SerialPort ("Executing sync script: $perforceSyncScript")
Write-SerialPort ("----------------------------------------------------------------------------")
Write-SerialPort (" ")

Exec-Then-SerialPort { Invoke-Expression $perforceSyncScript }

# Print the new last synced changelist
$lastChangeList = p4 changes -m1 //depot/main/SalesSuite...@$Env:P4CLIENT

Write-SerialPort (" ")
Write-SerialPort ("$originMachineName synced to changelist: $lastChangeList")
Write-SerialPort ("Synced Perforce changes!");

Write-SerialPort (" ")
Write-SerialPort ("Pull engine changes and compile...")


# Use cmd for git cmd because git pipes verbose messages in the error stream.
# See: https://stackoverflow.com/questions/57279007/error-when-pulling-from-powershell-script

# Switch to the correct engine branch. Note this only works if the branch was already synced once. 
Exec-Then-SerialPort { Set-Location $pathToEngine; & cmd /c 'git checkout' $engineBranchToSync '2>&1' }
Exec-Then-SerialPort { Set-Location $pathToEngine; & cmd /c 'git pull 2>&1' }

# Compile the engine
Exec-Then-SerialPort { & $msBuild $engineSolutionPath '-target:Engine\UE4' '-property:Platform=Win64;Configuration="Development Editor"' '-verbosity:normal' }

 # Shutdown machine
 if ($shutdown) {
    Write-SerialPort ("Done! Will shutdown now.")
    Stop-Transcript
    Start-Sleep 5 ; Stop-Computer
}