# Build and Sync Pipeline

## Goal

This Jenkins pipeline is used to sync the latest perforce changes on the masterbuild machine and use it as a "blueprint" to clone new build machines from

## Parameters

### Source Sync Machine
The source machine that syncs perforce changes and is used to clone new machines from. DefaultValue is buildmaster.

### Perforce Sync Script
Script that is run at startup of the Source Sync Machine.
By default it will sync everythin under //depot/main/SalesSuite. 