#!/usr/bin/python3

import re
import subprocess
import click
import datetime
import time
import logging


MAX_CONSECUTIVE_ERR = 20
MIN_WAIT_TIME = datetime.timedelta(seconds=5)

FORMAT = '[%(asctime)-15s :: %(levelname)s] %(message)s'
logging.basicConfig(format=FORMAT, level=logging.INFO)
logger = logging.getLogger('detect_end_serial')

re_start = re.compile(r"Specify --start=(?P<start>\d+) in the next")


def main(machine_name, end_string, gcloud):
    start = 0
    err_n = 0
    end_detected = False
    fail_detected = False
    while True:
        err_detected = False
        t0 = datetime.datetime.now()
        sub = subprocess.Popen(
            [gcloud, "compute", "instances", "get-serial-port-output", machine_name,
             "--zone=europe-west1-b", "--start={}".format(start)],
            universal_newlines=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        out = sub.stdout.read().strip()
        if out:
            for line in out.split('\n'):
                print(line)
                if "Failure! Will shutdown now." in line:
                    fail_detected = True
                for string in end_string:
                    if string in line:
                        end_detected = True
                        break
            if fail_detected:
                exit(2)

        err = sub.stderr.read().strip()
        if err:
            for line in err.split('\n'):
                match = re_start.search(line)
                if match is not None:
                    start = int(match.groupdict()['start'])
                else:
                    logger.error(line)
                    err_detected = True
        if err_detected:
            err_n += 1
            if err_n >= MAX_CONSECUTIVE_ERR:
                exit(1)
            t1 = datetime.datetime.now()
            if t1 - t0 < MIN_WAIT_TIME:
                sleep_time = (MIN_WAIT_TIME - (t1 - t0)).total_seconds()
                logger.info("About to sleep %s sec before retrying...", sleep_time)
                time.sleep(sleep_time)
        else:
            err_n = 0
        if end_detected:
            break


@click.command()
@click.option(
    "--machine-name",
    help="The name of the machine.",
)
@click.option(
    "--end-string",
    multiple=True,
    default="Already initialized. Skipping...",
    help="Ends the program when it detects that string."
)
@click.option(
    "--gcloud",
    type=click.Path(exists=True),
    help="Path to the gcloud binary",
)
def cli(machine_name, end_string, gcloud):
    main(machine_name, end_string, gcloud)



if __name__ == '__main__':
    cli()
