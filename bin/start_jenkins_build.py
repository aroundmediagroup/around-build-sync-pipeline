import logging

import time
import click
import requests
from bs4 import BeautifulSoup

#Base Urls
JENKINS_URL = "http://jenkins.vr-tual.media:8080"
CRUMB_URL = "{}/crumbIssuer/api/json"
START_BUILD_URL = "{}/job/{}/build"
START_BUILD_URL_WITH_PARAMS = "{}/job/{}/buildWithParameters?{}"
JOB_IN_QUEUE_URL = "{}/queue/item/{}/api/json"
JOB_URL = "{}/job/{}/{}/api/json"

#Wait time in seconds
WAIT_TIME_BUILD_QUEUE = 3
WAIT_TIME_BUILD_STATUS = 60

#Logger setup
FORMAT = '[%(asctime)-15s :: %(levelname)s] %(message)s'
logging.basicConfig(format=FORMAT, level=logging.INFO)
logger = logging.getLogger('start_jenkins_build')

def get_crumb(jenkins_url, auth, session):
    """
    Returns the crumb token from jenkins. 
    """
    crumb_response = session.get(url=CRUMB_URL.format(jenkins_url), auth=auth)
    if crumb_response.status_code != 200:
        soup = BeautifulSoup(crumb_response.text, 'html.parser')
        logger.error(
            "'Operation failed! Reason:\n%s",
            soup.text.strip()
        )
        exit(1)
    return crumb_response.json()

def get_post_headers(crumb):
    """
    Returns the headers used in post calls.
    """
    return {
        crumb['crumbRequestField']: crumb['crumb'],
        "Content-Type": "application/x-www-form-urlencoded",
    }

def start_jenkins_build(job, build_parameters, jenkins_url, auth, session, crumb):
    """
    Starts a new jenkins build. Returns the url to the location of the build in the queue list
    """
    logger.info("Starting build for job: %s", job)
    if build_parameters is None:
        start_build_url = START_BUILD_URL.format(jenkins_url, job)
    else:
        start_build_url = START_BUILD_URL_WITH_PARAMS.format(
            jenkins_url, job, build_parameters)
    logger.info("Calling url: %s", start_build_url)
    response = session.post(url=start_build_url, auth=auth, headers=get_post_headers(crumb))
    if response.status_code != 201:
        logger.error('Error returned: ' + response.text)
        soup = BeautifulSoup(response.text, 'html.parser')
        logger.error(
            "'Operation failed! Reason:\n%s",
            soup.text.strip()
        )
        exit(2)
    elif 'Location' in response.headers:
            return response.headers['Location']
    else:
        logger.error('Location header not in response')
        exit(3)

def get_build_in_queue_pipeline_nr_recursive(job, build_queue_nr, jenkins_url, auth, session, crumb, nr_attempts_remaining = 50):
    """
    Gets the build in queue info until it's in execution. Once in execution returns the build nr. 
    """
    logger.info("Getting info of job %s in queue %s...", job, build_queue_nr)
    job_in_queue_url = JOB_IN_QUEUE_URL.format(jenkins_url, build_queue_nr)
    response = session.post(url=job_in_queue_url, auth=auth, headers=get_post_headers(crumb))
    if response.status_code != 200:
        soup = BeautifulSoup(response.text, 'html.parser')
        logger.error(
            "'Operation failed! Reason:\n%s",
            soup.text.strip()
        )
        exit(4)

    json_data = response.json()
    if 'executable' not in json_data:
        if nr_attempts_remaining > 0:
            logger.info("job %s in queue %s is still in queue.", job, build_queue_nr)
            time.sleep(WAIT_TIME_BUILD_QUEUE)
            return get_build_in_queue_pipeline_nr_recursive(job, build_queue_nr, jenkins_url, auth, session, crumb, nr_attempts_remaining - 1)
        else:
            logger.error("Reached max attempts of checking if the job is still in the queue.")
            exit(5)
    executable = json_data['executable']
    if 'number' not in executable:
        logger.error("Operation failed! Reason: job number was not found in executor.")
        exit(6)
    else:
        logger.info("job %s in queue %s is in execution.", job, build_queue_nr)
        return executable['number']

def wait_for_build_to_complete_recursive(job, build_nr, jenkins_url, auth, session, crumb, nr_attempts_remaining = 60):
    """
    Get's the build status until it completes. Exits if the build doesn't end in success. 
    """
    logger.info("Getting status of job execution: %s.", job)
    job_url = JOB_URL.format(jenkins_url, job, build_nr)
    response = session.post(url=job_url, auth=auth, headers=get_post_headers(crumb))
    if response.status_code != 200:
        soup = BeautifulSoup(response.text, 'html.parser')
        logger.error(
            "'Operation failed! Reason:\n%s",
            soup.text.strip()
        )
        exit(7)
    json_data = response.json()
    if 'result' in json_data and 'building' in json_data and json_data['result'] is not 'null' and json_data['building'] is False:
        logger.info("Job %s - %s completed with result: %s.", job, build_nr, json_data['result'])
        if json_data['result'] != 'SUCCESS':
            exit(8)
        return
    else:
        if nr_attempts_remaining > 0:
            logger.info("Job %s - %s is in execution.", job, build_nr)
            time.sleep(WAIT_TIME_BUILD_STATUS)
            wait_for_build_to_complete_recursive(job, build_nr, jenkins_url, auth, session, crumb, nr_attempts_remaining - 1)
        else:
            logger.error("Reached max attempts of getting the completed status for a build.")
            exit(9)

def main(job, build_parameters, jenkins_url, jenkins_auth):
    logger.info("Goal: Starting and monitoring the following build: %s.", job)
    with requests.session() as session:
        crumb = get_crumb(jenkins_url,
                          jenkins_auth, session)

        #Split the returned url and get the build queue nr from it
        build_queue_nr = start_jenkins_build(job, build_parameters, jenkins_url, jenkins_auth, session, crumb).split("/")[-2]
        logger.info("Job's in queue, nr: %s", build_queue_nr)
        build_number = get_build_in_queue_pipeline_nr_recursive(job, build_queue_nr, jenkins_url, jenkins_auth, session, crumb)
        wait_for_build_to_complete_recursive(job, build_number, jenkins_url, jenkins_auth, session, crumb)

# Click Parameters
@click.command()
@click.option(
    "--job",
    help="The name of the job for which you want to start a build.",
)
@click.option(
    "--build-parameters",
    help="The build's parameters url encoded",
)
@click.option(
    "--jenkins-url", default=JENKINS_URL,
    help="The URL of the Jenkins server. Default: {}".format(JENKINS_URL),
)
@click.option(
    "--jenkins-auth",
    help="The auth for Jenkins, formatted as 'user:token'."
)
def cli(job, build_parameters, jenkins_url, jenkins_auth):
    """Starts a build."""
    main(job, build_parameters,
         jenkins_url, tuple(jenkins_auth.split(':')))

if __name__ == "__main__":
    cli()