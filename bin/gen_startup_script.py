#!/usr/bin/python3

import os
import textwrap

import click

def main(perforce_sync_script, ms_build_path, path_to_engine, shutdown, p4_auth, template, output_script):
    os.makedirs(os.path.dirname(output_script), exist_ok=True)
    with open(template, 'r') as template_file, open(output_script, 'w') as output_file:
        output_file.write(textwrap.dedent('''\
            $perforceSyncScript = "{perforce_sync_script}"
            $shutdown = {shutdown}
            $p4User = "{p4_user}"
            $p4Password = "{p4_password}"
            $msBuild = "{ms_build_path}"
            $pathToEngine = "{path_to_engine}"

            '''.format(
                perforce_sync_script=perforce_sync_script,
                shutdown="$TRUE" if shutdown else "$FALSE",
                p4_user=p4_auth[0],
                p4_password=p4_auth[1],
                ms_build_path = ms_build_path,
                path_to_engine = path_to_engine,
            )
        ))
        output_file.write(template_file.read())

@click.command()
@click.option(
    "--perforce-sync-script",
    help="perforce sync script.",
)
@click.option(
    "--ms-build-path",
    help="Path to msbuild executable.",
)
@click.option(
    "--path-to-engine",
    help="Path to the root engine folder.",
)
@click.option(
    "--shutdown/--online",
    default=True,
    help="Should the machine be shut down or left online? Default: shutdown",
)
@click.option(
    "--p4-auth",
    help="The auth for Perforce, formatted as 'user:token'."
)
@click.option(
    "--template",
    type=click.Path(exists=True, file_okay=True, dir_okay=False, resolve_path=True, readable=True),
    help="Path to the template script file.",
)
@click.option(
    "--output-script",
    type=click.Path(exists=False, file_okay=True, dir_okay=False, resolve_path=True, writable=True),
    help="Path to the output file.",
)
def cli(perforce_sync_script, ms_build_path, path_to_engine, shutdown, p4_auth, template, output_script):
    main(perforce_sync_script, ms_build_path, path_to_engine, shutdown, tuple(p4_auth.split(':')), template, output_script)


if __name__ == "__main__":
    cli()
